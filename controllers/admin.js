const student = require('../models/student');
const studentClass = require('../models/studentClass');
const studentReport = require('../models/studentReport');

exports.addName = (req,res,next) => {
    var obj ={
        studentName : req.body.studentName
    }
    student(obj).save()
    .then(result => {
        res.send('Name added Successfully');
    }).catch(error => {
        res.send('Error');
    })
}

exports.addClassName = (req,res,next) => {
    var obj = new studentClass({
        studentId : req.body.studentId,
        class : req.body.class
    })
    obj.save()
    .then(result => {
        res.send('Class added Successfully');
    }).catch(error => {
        res.send('Error');
    })
}

exports.name = (req,res,next) => {
    student.findOne({_id : req.body._id})
    .then(result => {
        res.send(result);
    }).catch(err => {
        res.send('Error');
    })
}

exports.class = (req,res,next) => {
    studentClass.findOne({_id : req.body._id})
    .then(result => {
        res.send(result);
    }).catch(err => {
        res.send('Error');
    })
}

exports.report = (req,res,next) => {
    student.findOne({_id:req.body.id})
    .then(result => {
        studentClass.findOne({studentId : req.body.id})
        .then(result1 =>{
            var obj = {
                studentId:result._id,
                class : result1.class,
                name: result.studentName
            }
            studentReport(obj).save()
            .then(result2 => {
                res.send(result2);
            }).catch(err => {
                res.send('Error');
            })
        })
    })
}