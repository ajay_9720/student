const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();

const adminRoutes = require('./routes/admin');

mongoose.connect('mongodb://127.0.0.1:27017/stud', { useNewUrlParser: true })  
.then(() => {            
    console.log
    (`Succesfully Connected to the Mongodb Database at URL : mongodb://127.0.0.1:27017/student`); 
       })        
       .catch(() => {            
           console.log
           (`Error Connecting to the Mongodb Database at URL : mongodb://127.0.0.1:27017/student`);        
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended : true
}))

app.use(adminRoutes);

app.listen(3000,function(req,res){
    console.log('App is running on port 3000');
})