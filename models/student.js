const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const studentSchema = new Schema({
    studentName : String
})

module.exports = mongoose.model('student',studentSchema);