const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const studentReportSchema =new Schema({

    studentId : {type : Schema.Types.ObjectId,ref: 'student'},
    // class : {type : Schema.Types.ObjectId,ref: 'studentClass'},
    name : String,
    class: String

})

module.exports = mongoose.model('report',studentReportSchema);