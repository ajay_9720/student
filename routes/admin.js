const express = require('express');
const router = express.Router();

const studentController = require('../controllers/admin');

router.post('/addName',studentController.addName);
router.post('/addClassName',studentController.addClassName);

router.get('/name',studentController.name);
router.get('/class',studentController.class);
router.post('/report',studentController.report);

module.exports = router;