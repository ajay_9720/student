const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const studentClassSchema = new Schema({

    studentId : {type:Schema.Types.ObjectId,ref: 'student'},
    class : String,

})

module.exports = mongoose.model('class',studentClassSchema);